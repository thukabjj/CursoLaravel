<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::group(['prefix'=>'eloquent','as'=>'eloquent.'],function(){
    Route::get('clients','EloquentClientsController@index')->name('clients.list');
});
Route::group(['prefix'=>'admin','as'=>'admin.'],function(){
    Route::get('client','ClientsController@create');
    Route::post('client','ClientsController@store')->name('client.store');
});
Route::group(['prefix'=>'','as'=>'site.'],function(){
    Route::get('client','SiteClientsController@create');
    Route::post('client','SiteClientsController@store')->name('client.store');
});

/*Route::get('/minharota',function(){
    return view('helloworld');
});
Route::get('minharota/rota1',function(){
    return view('helloworld1');
});
Route::get('client',function(){
    return view('client');
});
Route::post('client',function(Request $request){
    return $request->get('value');
})->name('client.store');

Route::get('client/{id}/{name?}',function($id,$name = "Fulano"){
    return view('client-name')->with('id',$id)->with('name',$name)->with('conteudo','teste');
});
Route::get('if-for',function(){
    return view('if-for');
});*/


