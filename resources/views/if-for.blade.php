<?php 
$num = 20;
?>

{{"Exemplo de estrutura condicional"}}

@if($num < 20)
    <p>Numero é menor que 20</p>
@elseif($num ==20)
    <p>Numero é igual a 20</p>
@else 
    <p>Numero é maior que 20</p>
@endif


{{"Exemplo de estrutura de repetição"}}

@for($i = 0; $i < 20;$i++)
    <p>Valor: {{$i}}</p> 
@endfor

<?php 
$k = 0;
?>
{{"WHILE"}}

@while($k <20)
    <p>Valor: {{$k}}</p> 
    <?php $k++?>
@endwhile

<?php 
$array =[1,2,3,4,5];
?>
{{"FOREACH"}}

@foreach($array as $value)
    <p>Chave{{$loop->index}}, Valor: {{$value}}</p>
@endforeach
<?php 
$array =[];
?>
@forelse($array as $value)
    <p>Chave{{$loop->index}}, Valor: {{$value}}</p>
@empty
    <p>Nenhum elemento Encontrado</p>
@endforelse